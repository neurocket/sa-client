import axios from 'axios';

const apiParams = {
  protocol: 'http',
  hostName: window.location.hostname,
  port: 8090,
  get url() {
    return `${this.protocol}://${window.location.hostname}:${this.port}`;
  },
};

const api = axios.create({
  baseURL: apiParams.url,
});

export default api;