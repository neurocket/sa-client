export default function(length, content) {
  return new Array(length).fill(content);
}