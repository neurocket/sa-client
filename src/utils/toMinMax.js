export default function(input) {
  if (+input.value < +input.min) {
    input.value = input.min;
  } else if (+input.value > +input.max) {
    input.value = input.max;
  }
  return +input.value;
}